package br.com.conductor.app.standards;

/**
 * $Revision: 237 $ $Author: rayan_rpg@hotmail.com $ $Date: 2011-02-14 11:03:12
 * -0300 (seg, 14 fev 2011) $
 *
 * @author Rayan
 *
 * Comandos SQL executeQuery apenas para select os outros use o executeUpdate.
 */
public class StringTable
{

    public static final String NOT_AVAIBLE = "N/A";
    public static final String JCOMBO_DEFAULT = "Selecione";
    public static final String EMPTY_STRING = "";
    public static String HW = "Hardware";
    public static String SW = "Software";
    public static String HWSW = HW + "&" + SW;
    public static final int JOPTIONPANE_YES = 0;
    public static final int JOPTIONPANE_NO = 1;
    /**
     * Others Strings
     */
    public static String[] SITUATION =
    {
        "Aguardando Autorização",
        "Aguardando Peça(s)",
        "Em Bancada",
        "Concluído",
        "Entregue",
        "Devolvido"
    };
    public static final String[] DATABASE_STATUS =
    {
        "Conectado",
        "Desconectado"
    };
    public static String FLOAT_DEFAULT = "0.0";
    public static String MOTHERBOARD = "Placa Mãe";
    public static String INTEL;

    public static String getEmptyStringDefault()
    {
        return EMPTY_STRING;
    }

    public static String getNotAvaible()
    {
        return NOT_AVAIBLE;
    }

    public static String getJComboDefault()
    {
        return NOT_AVAIBLE;
    }

//    /**
//     * Registra a App
//     */
//    public static final String REGISTRE_APP = "REPLACE INTO install VALUES (?,?,?,?,?,?,?,?,?)";
//            // bancoInstalado=?,stationMAC=?,StationMBSerial=?,Empresa=?,stationHDSerial=?,NumEstacoes=?,licenseType=?,registeredFor=?";
    /**
     * `bancoInstalado` varchar(5) NOT NULL default 'false', `statioMAC`
     * varchar(50) NOT NULL, `StationMBSerial` varchar(50) NOT NULL, `Empresa`
     * varchar(50) default NULL, `stationHDSerial` varchar(50) NOT NULL,
     * `NumEstacoes` int(50) NOT NULL, `licenseType` varchar(10) NOT NULL,
     * `registeredFor` varchar(30) NOT NULL,
     */
//
//      /**
//     * Registra a App
//     */
//    public static final String READ_APP_LICENSE_DATA = "SELECT `stationMAC`, `StationMBSerial`, `Empresa`, `stationHDSerial`, `NumEstacoes`, `licenseType`, `registeredFor`, `licenciado` FROM install";
//
//
//
//
//    /**
//     * Le a tabela de Usuarios
//     */
//    public static final String READ_USERS = "SELECT * FROM users";
//    /**
//     * Atualiza a Tabela de Instalacao
//     */
//    public static final String INSTALL = "UPDATE install SET bancoInstalado = '1'";
//    /**
//     * Le a Tabela de Instalacao
//     */
//    public static final String READ_INSTALL = "SELECT * FROM install";
    /**
     * Cria a Database
     */
//    public static final String CREATE_DB = "CREATE DATABASE IF NOT EXISTS " + Config.DATABASE;
    // *************************** Status da Database **********************/
}
