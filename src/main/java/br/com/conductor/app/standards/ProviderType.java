/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.standards;

/**
 *
 * @author Rayan
 */
/**
 * Tipo do Provedor
 */
public enum ProviderType
{

    /**
     * Provedor MySQL
     */
    MySql, /**
     * Provedor MsSql
     */
    MsSql

}
// =========================================================
