/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.standards;

import br.com.conductor.model.entity.Item;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author William
 */
public class ItemTableModel extends AbstractTableModel
{

    private List<Item> linhas;
// Constantes representando o índice das colunas
    private static final int QUANTIDADE = 0;
    private static final int DESCRICAO = 1;
    private static final int VALORUNITARIO = 2;
    private static final int TOTAL = 3;

    // Cria um SocioTableModel sem nenhuma linha
    public ItemTableModel()
    {
        linhas = new ArrayList<>();
    }

    // Cria um SocioTableModel contendo a lista recebida por parâmetro
    public ItemTableModel(List<Item> listaDeItens)
    {
        linhas = new ArrayList<>(listaDeItens);

    }

    String[] colunas = new String[]
    {
        "Quantidade",
        "Descrição",
        "Valor Unitário",
        "Total"

    };
    
        public List<Item> recuperarItensTabela() {  
        return this.linhas;  
    }  
//
//    /**
//     * O código acima irá altera o valor no ArrayList mas se você precisar que
//     * os dados também sejam imediatamente alterados no banco de dados por
//     * exemplo, você vai precisar adicionar um TableModelListener ao seu model
//     * que executará o método tableChanged toda vez que os dados da JTable forem
//     * alterados. Adicionaremos eles no construtor:
//     */
//    public ItemTableModel()
//    {
//        dados = new ArrayList<>();
//
//        this.addTableModelListener((TableModelEvent tme) ->
//        {
//            int linha = tme.getFirstRow();
//            Item p = dados.get(linha);
//            // aqui você atualiza no banco ou em outro lugar qualquer
//        });
//    }

//    public PecaTableModel()
//    {
//        dados = new ArrayList<>();
//    }
    public void addRow(Item i)
    {
        // Adiciona o registro.
        this.linhas.add(i);

        // Pega a quantidade de registros e subtrai 1 para
        // achar o último índice. A subtração é necessária
        // porque os índices começam em zero.
        int ultimoIndice = getRowCount() - 1;

        // Notifica a mudança.
        fireTableRowsInserted(ultimoIndice, ultimoIndice);
        //this.fireTableDataChanged();
    }

    @Override
    public String getColumnName(int num)
    {
        return this.colunas[num];
    }

    @Override
    public int getRowCount()
    {
        return linhas.size();
    }

    /**
     *
     *
     * That solution works with header sorting and updates automatically the
     * table (i.e.: it is not required to rebuid it).
     *
     * The "Collecions.sort in reverse order" ennables to avoid
     * IndexOutOfBondException.
     *
     * private void deleteSelectedRows() { int[] selectedRows =
     * table.getSelectedRows(); table.clearSelection();
     *
     * // get model rows List<Integer> selectedModelRows = new
     * LinkedList<Integer>(); for (int i =`enter code here` 0; i <
     * selectedRows.length; i++) {
     * selectedModelRows.add(table.convertRowIndexToModel(selectedRows[i])); }
     *
     * Collections.sort(selectedModelRows, Collections.reverseOrder());
     *
     * for (int selectedModelRow : selectedModelRows) {
     * tableModel.removeRow(selectedModelRow);
     * tableModel.fireTableRowsDeleted(selectedModelRow, selectedModelRow); } }
     *
     *
     */
    /**
     *
     * @param linhas
     * @param lista
     */
    public void removeRow(int[] linhas, List lista)
    {

        for (int i = 0; i < linhas.length; i++)
        {
            this.linhas.remove(linhas[i] - i);
            //this.fire..
            fireTableRowsDeleted(linhas[i] - i, linhas[i] - i);
            lista.remove(linhas[i] - i);
        }
        //this.linhas.remove(linha);

    }

    @Override
    public boolean isCellEditable(int linha, int coluna)
    {
        return true;
    }

    public void setValueAt(Object valor, int linha, int coluna)
    {
        if (valor == null)
        {
            return;
        }

        switch (coluna)
        {
            case QUANTIDADE:
                linhas.get(linha).setQuantidade(Integer.parseInt((String) valor));
                break;
            case DESCRICAO:
                linhas.get(linha).setDescrição((String) valor);
                break;
            case VALORUNITARIO:
                linhas.get(linha).setValorUnitario(Double.parseDouble((String) valor));
                break;
            case TOTAL:
                linhas.get(linha).setTotal(Double.parseDouble((String) valor));
                break;
        }
        this.fireTableRowsUpdated(linha, linha);
    }
// Remove todos os registros.

    public void limpar()
    {
        // Remove todos os elementos da lista de sócios.
        linhas.clear();

        // Notifica a mudança.
        fireTableDataChanged();
    }

    public Item get(int linha)
    {
        return this.linhas.get(linha);
    }

    @Override
    public int getColumnCount()
    {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int linha, int coluna)
    {
        switch (coluna)
        {
            case 0:
                return linhas.get(linha).getQuantidade();
            case 1:
                return linhas.get(linha).getDescrição();
            case 2:
                return linhas.get(linha).getValorUnitario();
            case 3:
                return linhas.get(linha).getTotal();
        }
        return null;
    }

}
