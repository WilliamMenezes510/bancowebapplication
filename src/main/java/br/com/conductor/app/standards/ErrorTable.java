/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.standards;

/**
 * $Revision: 894 $ $Author: RayanRPG@gmail.com $ $Date: 2012-05-15 22:13:22
 * -0300 (Ter, 15 Mai 2012) $
 *
 * @author Rayan
 */
public class ErrorTable
{

    /**
     *
     */
    private static String MYSQL_PROCESS_ERROR = "0x001";

    /**
     *
     * @param errorCode
     * @return 1 - MySQL Process Not Found.
     */
    public static String throwError(int errorCode)
    {
        String msg;
        switch (errorCode)
        {
            case 1: // mysql process error
            {
                msg = "Erro de Código: " + ErrorTable.getMYSQL_PROCESS_ERROR() + "(MySQL Process Not Found)";
                break;
            }
            default:
                msg = "null";
        }
        return msg;
    }

    /**
     * @return the MYSQL_PROCESS_ERROR
     */
    public static String getMYSQL_PROCESS_ERROR()
    {
        return MYSQL_PROCESS_ERROR;
    }

    /**
     * @param aMYSQL_PROCESS_ERROR the MYSQL_PROCESS_ERROR to set
     */
    public static void setMYSQL_PROCESS_ERROR(String aMYSQL_PROCESS_ERROR)
    {
        MYSQL_PROCESS_ERROR = aMYSQL_PROCESS_ERROR;
    }

}
