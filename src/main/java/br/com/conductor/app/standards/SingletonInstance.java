/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.standards;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import org.apache.log4j.Logger;

/**
 *
 * @author Rayan
 */
public class SingletonInstance
{

    private static FileLock lock;
    private static FileChannel channel;
    private static File f;
    private static final Logger _log = Logger.getLogger(SingletonInstance.class);

    /**
     *
     * @param args
     */
    public static void main(String args[])
    {
        try
        {
            setF(new File("key"));
            if (getF().exists())
            {
                getF().delete();
            }
            setChannel(new RandomAccessFile(getF(), "rw").getChannel());
            setLock(getChannel().tryLock());

            if (getLock() == null)
            {
                getChannel().close();
                throw new RuntimeException("Apenas uma Instance do Programa pode Ser Criada");
            }
            Thread shutdown = new Thread(new Runnable()
            {

                @Override
                public void run()
                {
                    unlock();
                }
            });

            Runtime.getRuntime().addShutdownHook(shutdown);
            _log.info("rodando..");
            while (true)
            {
            }
        }
        catch (IOException | RuntimeException e)
        {
            throw new RuntimeException("Apenas uma Instance do Programa pode Ser Criada", e);
        }
    }

    /**
     *
     *
     *
     */
    public static void unlock()
    {
        try
        {

            if (getLock() != null)
            {
                getLock().release();
                getChannel().close();
                getF().delete();
            }
        }
        catch (IOException e)
        {
        }
    }

    /**
     * @return the lock
     */
    public static FileLock getLock()
    {
        return lock;
    }

    /**
     * @param aLock the lock to set
     */
    public static void setLock(FileLock aLock)
    {
        lock = aLock;
    }

    /**
     * @return the channel
     */
    public static FileChannel getChannel()
    {
        return channel;
    }

    /**
     * @param aChannel the channel to set
     */
    public static void setChannel(FileChannel aChannel)
    {
        channel = aChannel;
    }

    /**
     * @return the f
     */
    public static File getF()
    {
        return f;
    }

    /**
     * @param aF the f to set
     */
    public static void setF(File aF)
    {
        f = aF;
    }
}
