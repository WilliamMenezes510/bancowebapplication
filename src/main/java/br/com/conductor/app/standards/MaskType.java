/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.standards;

/**
 *
 * @author William
 */
public class MaskType
{

    /**
     *
     */
    public static final int TELEFONE = 0;
    /**
     *
     */
    public static final int CEP = 1;
    /**
     *
     */
    public static final int DATA = 2;
    /**
     *
     */
    public static final int CPF = 3;
    /**
     *
     */
    public static final int CNPJ = 4;
    /**
     *
     */
    public static final int NULL = 5;
    /**
     *
     */
    public static final int MOEDA = 6;
    public static final int SEARCH= 7;
}
