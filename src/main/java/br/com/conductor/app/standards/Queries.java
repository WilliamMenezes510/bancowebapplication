/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.standards;

/**
 *
 * @author William
 */
public class Queries
{

    /**
     * SQL Queries
     */
    
    //********************** INSERTS ***************************************
    private static final String INSERT_USER = "INSERT INTO `usuarios`(login,senha,nivelAcesso,nomeCompleto,endereco,cargo) VALUES (?,?,?,?,?,?)";
    private static final String INSERT_ENTRADA = "INSERT INTO `entradaequip`(numentrada,data,usuario,tipoentrada,situacao,tecnico,perifericos,observacoes,marca,equipamento,quantidade,responsavel,defeitos,hora,tipodefeito) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String INSERT_TECNICO = "INSERT INTO `tecnicos` (nome,sobrenome) VALUES (?,?)";
    public static final String INSERT_MARCA = "INSERT INTO `marcas`(marca,tipo) VALUES (?,?)";

    // ************* SELECTS *********************************************************
    public static final String SELECT_USER_PASS = "SELECT `senha` FROM usuarios WHERE login=?";
    public static final String SELECT_ESTADOS = "SELECT * FROM estados order by id";
    public static final String SELECT_MARCA = "SELECT * FROM marcas order by id";
    public static final String SELECT_TECNICOS = "SELECT * FROM tecnicos order by id";
    public static String SELECT_DEFEITOS = "SELECT * FROM defeitos order by id";
    public static final String SELECT_USER = "SELECT * from usuarios WHERE  login=? AND senha=?";
    public static final String SELECT_EQUIPAMENTOS = "SELECT * FROM equipamentos ORDER BY id ASC";
    public static final String SELECT_CIDADES = "SELECT * FROM cidades order by id";
    public static final String SELECT_ACC_LVL = "SELECT accesslevel FROM users WHERE login =?";
    public static final String SELECT_MAX_ID = "SELECT MAX(`id`) FROM entradaequip";
    public static String SELECT_SERVICOS = "SELECT * FROM servicos order by id";
    public static String SELECT_PECAS = "SELECT * FROM pecas order by id";
    public static String SELECT_TIPOS = "SELECT * FROM tipos order by id";
    public static String SELECT_FABRICANTES = "SELECT * FROM fabricantes order by id";
    public static String SELECT_SOQUETES = "SELECT * FROM soquetes order by id";
    public static String SELECT_MODELOS = "SELECT * FROM modelos order by id";
    // ************* UPDATES **********************************************************
    public static final String UPDATE_USER_PASS = "UPDATE usuarios SET senha=? WHERE login=?";
    public static final String UPDATE_USER_ACCESS_LVL = "UPDATE usuarioss SET accesslvl=?  WHERE login=?";
    public static final String UPDATE_USER = "UPDATE usuarios SET (login,senha)WHERE login=? AND senha=?";
    public static final String UPDATE_ENTRADA = "UPDATE entrada SET situacao=? WHERE situacao=?";
    public static final String UPDATE_EQUIPMENT_SITUATION = "UPDATE entradaequip SET situacao=? WHERE numEntrada=?";
    // ********************* DELETES *********************************************
    public static final String DELETE_USER = "DELETE FROM usuarios WHERE id=? AND login =?";

    /**
     *
     * @return
     */
    public static String getInsertTecnico()
    {
        return INSERT_TECNICO;
    }

    /**
     *
     * @return
     */
    public static String getInsertUser()
    {
        return INSERT_USER;
    }

    public static String getInsertEntrada()
    {
        return INSERT_ENTRADA;
    }

    public static String getEmptyString()
    {
        return INSERT_ENTRADA;
    }

    public static String getSelectUser()
    {
        return SELECT_USER;
    }

    public static String getInserTecnico()
    {
        return INSERT_TECNICO;
    }

    public static String getDeleteUser()
    {
        return DELETE_USER;
    }

}
