/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.standards;

/**
 *
 * @author William
 */
public enum ItemType
{

    FABRICANTE,
    PEÇA,
    SOQUETE,
    SERVIÇO,
    ALL,
    TIPO,
    MODELO,
    MARCA, 
    TECNICO, DEFEITO, EQUIPAMENTO
}
