/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.conductor.app.standards;

/**
 *
 * @author William
 */
public class Paths
{
     /**
     * Directories
     */
    private static final String SOUND_DIRECTORY = "./som/";
    private static final String IMAGE_DIRECTORY = "./images/";
    private static final String SQL_DIRECTORY = "./database/";
    private static final String UPDATE_DIRECTORY = "./update/";
    private static final String CONFIG_DIRECTORY = "./config/";
    private static final String CONFIG_FILE = "./config/settings.properties";
    private static final String UPDATE_FILE = "./update/update.properties";
    private static final String LOG_FILE = "./log/meulog.log";

    
      /**
     *
     * @return
     */
    public static String getUpdateDirecory()
    {
        return UPDATE_DIRECTORY;
    }

    /**
     * @return
     */
    public static String getSoundDirectory()
    {
        return SOUND_DIRECTORY;
    }

    /**
     * @return
     */
    public static String getImageDirecroty()
    {
        return IMAGE_DIRECTORY;
    }

    /**
     * @return
     */
    public static String getSQLDirectory()
    {
        return SQL_DIRECTORY;
    }

    /**
     * @return
     */
    public static String getConfigDirectory()
    {
        return CONFIG_DIRECTORY;
    }
    
    
    /**
     * @return
     */
    public static String getConfigFile()
    {
        return CONFIG_FILE;
    }

    /**
     * @return the UPDATE_FILE
     */
    public static String getUpdateFile()
    {
        return UPDATE_FILE;
    }

    /**
     * @return the LOG_FILE
     */
    public static String getLogFile()
    {
        return LOG_FILE;
    }
}
