/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.init;

import br.com.conductor.app.managers.PropertiesManager;
import br.com.conductor.app.standards.Paths;
import br.com.conductor.app.utils.Util;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author William
 */
public class Config
{

    /**
     * Variaveis para Acesso da API
     */
    public static String API_HOST;
    public static String API_CONTEXT_PATH;
    public static String API_PROTOCOL;
    public static String API_RELATIVE_URL;
    public static String API_PORT;

    public static void main(String[] args)
    {
        Config.getInstance();
    }
    private static final Logger _log = Logger.getLogger(Config.class);
    private static Boolean enableLog;
    private static boolean _loaded;
    /**
     * Database
     */
    private static String databaseLogin;
    private static String databasePassword;
    private static String databaseName;
    private static String databaseHost;
    private static String databaseDriver;
    //levels
    private static int adminLevel;
    private static int operadorLevel;
    private static int gerenteLevel;
    private static int tecnicoLevel;
    //personalizacao
    private static String site;
    private static double systemVersion;
    private static String empresa;
    private static String databaseProtocol;
    private static String databasePort;
    private static String logo;
    /**
     *
     * Sons
     */
    private static boolean enableSound;
    private static String loginSound;
    private static String questionSound;
    private static String welcomeSound;
    private static String exitSound;
    private static String preExitSound;
    private static String preLoginSound;
    private static String unimplementedSound;
    private static String preRestartSound;
    private static String restartSound;
    private static String infoSound;
    private static String logoffSound;
    //developer
    private static boolean debug;
    private static boolean developer;
    private static boolean checaTempoOcioso;
    //rede
    private static int serverPort;
    private static int clientPort;
    private static String serverIp;
    private static int testPort;
    private static int testWait;
    private static String updateUrl;
    private static int tempoOcioso;
    private static int THREAD_DEFAULT_SLEEP = 1000;
    private static String systemPath;

    /**
     *
     */
    public boolean IMPEDIR_MINIMIZAR = false;

    private Config()
    {
        load();

    }

    /**
     *
     * @return
     */
    public static Config getInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder
    {

        protected static final Config INSTANCE = new Config();
    }

    /**
     *
     */
    public void loadPreferencies()
    {
//        if (isEnableLog())
//        {
//            _log.info("Carregando Preferencias ...");
//        }
//
//        ///JOption pane
//        UIManager.put("OptionPane.font", new Font("Tahoma", Font.BOLD, 14));
//        UIManager.put("OptionPane.foreground", Color.red);
//        UIManager.put("OptionPane.messageDialogTitle", "Mensagem");
//        UIManager.put("OptionPane.cancelButtonText", "Cancelar");
//        UIManager.put("OptionPane.yesButtonText", "Sim");
//        UIManager.put("OptionPane.noButtonText", "NÃ£o");
//
//        //File Chooser
//        UIManager.put("OptionPane.titleText", "Selecione um opÃ§Ã£o");
//        UIManager.put("FileChooser.lookInLabelMnemonic", new Integer('E'));
//        UIManager.put("FileChooser.lookInLabelText", "Examinar");
//        UIManager.put("FileChooser.saveInLabelText", "Salvar em");
//        UIManager.put("FileChooser.fileNameLabelMnemonic", new Integer('N'));  // N
//        UIManager.put("FileChooser.fileNameLabelText", "Nome do arquivo");
//        UIManager.put("FileChooser.filesOfTypeLabelMnemonic", new Integer('T'));  // T
//        UIManager.put("FileChooser.filesOfTypeLabelText", "Tipo");
//        UIManager.put("FileChooser.upFolderToolTipText", "Um nÃ­vel acima");
//        UIManager.put("FileChooser.upFolderAccessibleName", "Um nÃ­vel acima");
//        UIManager.put("FileChooser.homeFolderToolTipText", "Desktop");
//        UIManager.put("FileChooser.homeFolderAccessibleName", "Desktop");
//        UIManager.put("FileChooser.newFolderToolTipText", "Criar nova pasta");
//        UIManager.put("FileChooser.newFolderAccessibleName", "Criar nova pasta");
//        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");
//        UIManager.put("FileChooser.listViewButtonAccessibleName", "Lista");
//        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Detalhes");
//        UIManager.put("FileChooser.detailsViewButtonAccessibleName", "Detalhes");
//        UIManager.put("FileChooser.fileNameHeaderText", "Nome");
//        UIManager.put("FileChooser.fileSizeHeaderText", "Tamanho");
//        UIManager.put("FileChooser.fileTypeHeaderText", "Tipo");
//        UIManager.put("FileChooser.fileDateHeaderText", "Data");
//        UIManager.put("FileChooser.fileAttrHeaderText", "Atributos");
//        UIManager.put("FileChooser.openButtonToolTipText", "Abrir");
//        UIManager.put("FileChooser.openButtonAccessibleName", "Abrir");
//        UIManager.put("FileChooser.openButtonText", "Abrir");
//        UIManager.put("FileChooser.cancelButtonText", "Cancelar");
//        UIManager.put("FileChooser.cancelButtonToolTipText", "Cancela esta Janela");
//        UIManager.put("FileChooser.cancelButtonAccessibleName", "Cancelar");
//        setTema();
    }
//
//    /**
//     *
//     * @param requesterClass
//     */
//    private void setTema()
//    {
//        if (isDebug())
//        {
//            _log.info("Setando Tema Visual");
//        }
//        try
//        {
//            //Define O Tema Visual e o Texto do Pop UP
//            Properties props = new Properties();
//            props.put("logoString", "Sat");
//            props.put("licenseKey", "Sat");
//         //   AeroLookAndFeel.setCurrentTheme(props);
//            UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
//        }
//        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex)
//        {
//            _log.fatal(ex.getMessage());
//          //("Erro ao Carregar Skin ", ex);
//        }
//    }

    /**
     * LÃª a Versao Atual do Sistema
     */
    private void readSystemVersion()
    {
        File localFile = new File("./update/local.properties");//"C:\\Sat\\update\\local.properties");

        if (localFile.exists())
        {
            if (isDebug())
            {
                _log.info("Lendo Arquivo de Versao Local do Sistema...");
            }
            try
            {
                InputStream is = new FileInputStream(localFile);
                PropertiesManager arquivo = new PropertiesManager();
                arquivo.load(is);
                systemVersion = Double.parseDouble(arquivo.getProperty("version", "0.0"));
            }
            catch (IOException ex)
            {
                java.util.logging.Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            Util.showErrorMessage("O Arquivo" + localFile.getPath() + " Nao Existe!");

        }

    }

    /**
     * Carrega Todas as Propriedades do config.properties
     */
    private void load()
    {
        if (isDebug())
        {
            _log.info("Config: Carregando o Arquuivo de ConfiguraÃ§Ã£o...");
        }
        try
        {

            File arquivoConfig = new File(Paths.getConfigFile());
            if (!arquivoConfig.exists())
            {
                Util.showErrorMessage("o arquivo de configuracao  " + arquivoConfig.getName() + " nao existe!");
                System.exit(0);//finaliza o sistema.
            }
            else
            {

                InputStream is = new FileInputStream(arquivoConfig);
                PropertiesManager arquivo = new PropertiesManager();
                arquivo.load(is);

                /**
                 * Configs
                 */
                empresa = arquivo.getProperty("empresa", "desconhecida");
                systemPath = arquivo.getProperty("systemPath", "C:\\Sat\\");
                databaseLogin = arquivo.getProperty("login", "root");
                databasePassword = arquivo.getProperty("password", "root");
                databaseDriver = arquivo.getProperty("driver", "com.mysql.jdbc.Driver");
                databaseProtocol = arquivo.getProperty("URL", "jdbc:mysql://");
                databaseName = arquivo.getProperty("database", "sat");
                databasePort = arquivo.getProperty("databasePort", "3306");
                databaseHost = arquivo.getProperty("databaseHost", "localhost");
                serverPort = Integer.parseInt(arquivo.getProperty("serverPort", "7000"));
                clientPort = Integer.parseInt(arquivo.getProperty("clientPort", "7000"));
                adminLevel = Integer.parseInt(arquivo.getProperty("adminLvl", "500"));
                operadorLevel = Integer.parseInt(arquivo.getProperty("operadorLvl", "100"));
                gerenteLevel = Integer.parseInt(arquivo.getProperty("gerenteLvl", "250"));
                site = arquivo.getProperty("site", "www.sat.com.br");
                logo = arquivo.getProperty("logo", "logo.png");
                testPort = Integer.parseInt(arquivo.getProperty("TestPort", "80"));
                testWait = Integer.parseInt(arquivo.getProperty("TestWait", "2"));//2 seconds
                enableLog = Boolean.valueOf(arquivo.getProperty("log", "false"));
                // Configuracoes de Som
                enableSound = Boolean.valueOf(arquivo.getProperty("sound", "true "));
                exitSound = arquivo.getProperty("exit", "exitok.wav");
                preExitSound = arquivo.getProperty("preExit", "exit.wav");
                loginSound = arquivo.getProperty("loginSound", "loginok.wav");
                preLoginSound = arquivo.getProperty("preLogin", "login.wav");
                unimplementedSound = arquivo.getProperty("uniplemented", "implementar.wav");
                preRestartSound = arquivo.getProperty("preRestart", "restart.wav");
                restartSound = arquivo.getProperty("restartSound", "restartok.wav");
                welcomeSound = arquivo.getProperty("welcomeSound", "welcome.wav");
                questionSound = arquivo.getProperty("questionSound", "question.wav");
                infoSound = arquivo.getProperty("infoSound", "info.wav");
                logoffSound = arquivo.getProperty("logoffSound", "logoff.wav");
                //-------------------------------------------------------------------
                debug = Boolean.valueOf(arquivo.getProperty("debug", "true"));
                enableLog = Boolean.valueOf(arquivo.getProperty("log", "false"));

                developer = Boolean.valueOf(arquivo.getProperty("developer", "true"));
                checaTempoOcioso = Boolean.valueOf(arquivo.getProperty("developer", "true"));
//levels
                adminLevel = Integer.parseInt(arquivo.getProperty("NivelAdmin", "500"));//2 seconds

                gerenteLevel = Integer.parseInt(arquivo.getProperty("NivelGerente", "400"));//2 seconds
                tecnicoLevel = Integer.parseInt(arquivo.getProperty("NivelTecnico", "300"));//2 seconds
                operadorLevel = Integer.parseInt(arquivo.getProperty("NivelFuncionario", "200"));//2 seconds
                updateUrl = arquivo.getProperty("updateUrl", "http://rayan.googlecode.com/svn/trunk/Sat/release/");
                tempoOcioso = Integer.parseInt(arquivo.getProperty("tempoOcioso", "60"));//60 seconds

                //API CONFIG
                API_HOST = arquivo.getProperty("apihost", "desconhecida");
                API_PORT = arquivo.getProperty("apiport", "desconhecida");
                API_PROTOCOL = arquivo.getProperty("apiprotocol", "https");
                API_CONTEXT_PATH = arquivo.getProperty("apicontextpath", "desconhecida");
                API_RELATIVE_URL = arquivo.getProperty("apirelativeurl", "desconhecida");

                /**
                 * Port Checks
                 */
                if (serverPort <= 0 || serverPort > 65535)
                {
                    //   Util u = new Util();
                    Util.showErrorMessage("Erro na Configuracao as Portas do Programa.\n");
                    System.exit(0);
                }

                _loaded = true;
            }

        }
        catch (IOException | NumberFormatException e)
        {
            _log.fatal(e.getMessage());
            // ExceptionManager.ThrowException("Nao Foi Possivel Carregar o Arquivo:  de configuracao: ", e);
        }

        loadPreferencies();
        readSystemVersion();
    }

    /**
     *
     * @return
     */
    public String getDatabasePassword()
    {
        return databasePassword;
    }

    /**
     *
     * @return
     */
    public String getDatabaseLogin()
    {
        return databaseLogin;
    }

    /**
     *
     * @return
     */
    public String getDatabaseDriver()
    {
        return databaseDriver;
    }

    /**
     *
     * @return
     */
    public String getDatabaseHost()
    {
        return databaseHost;
    }

    /**
     *
     * @return
     */
    public String getDatabaseName()
    {
        return databaseName;
    }

    /**
     *
     * @return
     */
    public boolean isEnableSound()
    {
        return enableSound;
    }

    /**
     *
     * @return
     */
    public int getClientPort()
    {
        return clientPort;
    }

    /**
     *
     * @return
     */
    public String getServerIp()
    {
        return serverIp;
    }

    /**
     *
     * @return
     */
    public Boolean isEnableLog()
    {
        return enableLog;
    }

    /**
     *
     * @return
     */
    public int getServerPort()
    {
        return serverPort;
    }

    /**
     *
     * @param enableSound
     */
    public void setEnableSound(boolean enableSound)
    {
        Config.enableSound = enableSound;
    }

    /**
     *
     * @return
     */
    public boolean isDebug()
    {
        return debug;
    }

//   public String getUpdateServer()
//    {
//        return updateServer;
//    }
    /**
     *
     * @return
     */
    public boolean isLoaded()
    {
        return _loaded;
    }

    /**
     *
     * @return
     */
    public String getSite()
    {
        return site;
    }

    /**
     *
     * @return
     */
    public double getSystemVersion()
    {
        return systemVersion;
    }

    /**
     *
     * @return
     */
    public int getTestPort()
    {
        return testPort;
    }

    /**
     *
     * @return
     */
    public int getTestWait()
    {
        return testWait;
    }

    /**
     *
     * @return
     */
    public boolean isDeveloper()
    {
        return developer;
    }

    /**
     *
     * @return
     */
    public boolean checaTempoOcioso()
    {
        return checaTempoOcioso;
    }

    /**
     *
     * @return
     */
    public int getAdminLevel()
    {
        return adminLevel;
    }

    /**
     *
     * @return
     */
    public int getGerenteLevel()
    {
        return gerenteLevel;
    }

    /**
     *
     * @return
     */
    public int getOperadorLevel()
    {
        return operadorLevel;
    }

    /**
     *
     * @return
     */
    public int getTecnicoLevel()
    {
        return tecnicoLevel;
    }

    /**
     *
     * @return
     */
    public String getEmpresa()
    {
        return empresa;
    }

    /**
     *
     * @return
     */
    public String getDatabasePort()
    {
        return databasePort;
    }

    /**
     *
     * @return
     */
    public String getDatabaseProtocol()
    {
        return databaseProtocol;
    }

    /**
     *
     * @return
     */
    public String getExitSound()
    {
        return exitSound;
    }

    /**
     *
     * @return
     */
    public String getPreExitSound()
    {
        return preExitSound;
    }

    /**
     *
     * @return
     */
    public String getQuestionSound()
    {
        return questionSound;
    }

    /**
     *
     * @return
     */
    public String getWelcomeSound()
    {
        return welcomeSound;
    }

    /**
     *
     * @return
     */
    public String getLogo()
    {
        return logo;
    }

    /**
     *
     * @return
     */
    public String getLoginSound()
    {
        return loginSound;
    }

    /**
     *
     * @return
     */
    public String getPreLoginSound()
    {
        return preLoginSound;
    }

    /**
     *
     * @return
     */
    public String getPreRestartSound()
    {
        return preRestartSound;
    }

    /**
     *
     * @return
     */
    public String getRestartSound()
    {
        return restartSound;
    }

    /**
     *
     * @return
     */
    public String getUnimplementedSound()
    {
        return unimplementedSound;
    }

    /**
     *
     * @return
     */
    public String getInfoSound()
    {
        return infoSound;
    }

    /**
     *
     * @return
     */
    public String getUpdateUrl()
    {
        return updateUrl;
    }

    /**
     *
     * @return
     */
    public String getLogoffSound()
    {
        return logoffSound;
    }

    /**
     *
     * @return
     */
    public String getSystemPath()
    {
        return systemPath;
    }

    /**
     *
     * @return
     */
    public int getTempoOcioso()
    {
        return tempoOcioso;
    }

    /**
     * @return the THREAD_DEFAULT_SLEEP
     */
    public int getTHREAD_DEFAULT_SLEEP()
    {
        return THREAD_DEFAULT_SLEEP;
    }

    /**
     * @param aTHREAD_DEFAULT_SLEEP the THREAD_DEFAULT_SLEEP to set
     */
    public void setTHREAD_DEFAULT_SLEEP(int aTHREAD_DEFAULT_SLEEP)
    {
        THREAD_DEFAULT_SLEEP = aTHREAD_DEFAULT_SLEEP;
    }
}
