/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.managers;
import br.com.conductor.app.init.Config;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;

/**
 * $Revision$ $Author$ $Date$
 *
 * @author Rayan
 */
public class LogManager
{

    private static final Logger _log = Logger.getLogger(LogManager.class);

    public static void main(String[] args) throws IOException
    {
    }

    private LogManager()
    {
        configureLogger();

        if (!Config.getInstance().isEnableLog())
        {
            return;
        }

        if (Config.getInstance().isDebug())
        {
            _log.info("Inicializando o Log Manager..;");
        }

    }

    private void configureLogger()
    {
        System.out.println("Configurando logger");
        File pasta = new File("./log");
        File arquivo = new File("./log/meulog.log");
        try
        {
            if (pasta.exists())
            {
                pasta.delete();
                pasta.mkdir();
            }
            else
            {
                pasta.mkdir();
            }

            if (arquivo.exists())
            {
                //deleta log antigo
                arquivo.delete();

                //cria novo arquivo de log.
                arquivo.createNewFile();
            }
            else
            {
                arquivo.createNewFile();
            }

            String log4JPropertyFile = "./log/log4j.properties";
            Properties p = new Properties();

            try
            {
                p.load(new FileInputStream(log4JPropertyFile));
                PropertyConfigurator.configure(p);
                _log.info("Log4J Configurado com Sucesso!");
            }
            catch (IOException e)
            {
                _log.error("Erro ao Configurar o  Log4j!");

            }

            try
            {
                /* Cria um novo FileAppender baseado no layout padrÃ£o, 
                 * prÃ©-definido na constante TTCC_CONVERSION_PATTERN 
                 * da classe PatternLayout. */
                Appender fileAppender = new FileAppender(new PatternLayout(PatternLayout.TTCC_CONVERSION_PATTERN), arquivo.getAbsolutePath());
                _log.addAppender(fileAppender);

                //throw new Exception("?Loga esse no arquivo, Log4J!?");
            }
            catch (IOException e)
            {
                _log.error("?Oops, deu erro: ? + e.getMessage()");
            }
        }
        catch (IOException ex)
        {
           // java.util.logging.Logger.getLogger(AppManager.class.getName()).log(Level.SEVERE, null, ex);
            _log.error("?Oops, deu erro: ?" + ex.getMessage());
           // ExceptionManager.ThrowException("Erro ", ex);
        }
            // Cria um alimentador de arquivo que adiciona os dados
        //------------------------------------------------
        //boolean append = true;
        // FileHandler handler = new FileHandler("./log/meulog.log", append);

        //handler.setFormatter(new SimpleFormatter());
        // Adicione ao logger desejado
        //------------------------------------------------
        // Logger logger = Logger.getLogger("br.facol.lpoo");
        //  logger.addHandler(handler);
        //}
        //catch (IOException | SecurityException ex)
        //{
        //    Logger.getLogger(LogManager.class.getName()).log(Level.SEVERE, null, ex);
        //}
    }

    /**
     *
     * @return
     */
    public static LogManager getInstance()
    {
        return SingletonHolder._instance;
    }

    private static class SingletonHolder
    {

        protected static final LogManager _instance = new LogManager();
    }
}