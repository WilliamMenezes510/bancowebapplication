package br.com.conductor.app.utils;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

/**
 * http://www.java2s.com/Tutorial/Java/0260__Swing-Event/ListeningtoJTextFieldEventswithaDocumentListener.htm
 * Listening to JTextField Events with a DocumentListener
 */
public class JTextFieldDocumentListener
{

    private static int length = -1;
    //  private boolean ok = false;

//    public int getLength()
//    {
//        return length;
//    }
//
//    public void setLength(int length)
//    {
//        this.length = length;
//    }
    private JTextFieldDocumentListener()
    {
    }

    /**
     *
     * @param a
     * @param b
     */
    public static void verificaCampo(JTextField a, final JTextField b)
    {
        DocumentListener documentListener = new DocumentListener()
        {
            @Override
            public void changedUpdate(DocumentEvent documentEvent)
            {
                printIt(documentEvent);
            }

            @Override
            public void insertUpdate(DocumentEvent documentEvent)
            {
                printIt(documentEvent);
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent)
            {
                printIt(documentEvent);
            }

            private void printIt(DocumentEvent documentEvent)
            {
                DocumentEvent.EventType type = documentEvent.getType();
                String typeString = null;
                if (type.equals(DocumentEvent.EventType.CHANGE))
                {
                    typeString = "Change";
                }
                else if (type.equals(DocumentEvent.EventType.INSERT))
                {
                    typeString = "Insert";
                }
                else if (type.equals(DocumentEvent.EventType.REMOVE))
                {
                    typeString = "Remove";
                }
                System.out.print("Type : " + typeString);
                Document source = documentEvent.getDocument();

                int length = source.getLength();
                //JTextFieldDocumentListener jtfl = JTextFieldDocumentListener.getInstance();
                // jtfl.setLength(length);

                if (length >= 1)
                {
                    b.setEnabled(true);
                    System.out.println("Habilitando Campo: " + b.getName());
                }
                else if (length < 1)
                {
                    b.setEnabled(false);
                    System.out.println("Desabilitando Campo: " + b.getName());
                }
                System.out.println("Length: " + length);
            }
        };
        a.getDocument().addDocumentListener(documentListener);

    }
    /*
     * codigoField = new JTextField(4);
     * codigoField.getDocument().addDocumentListener(new DocumentListener() {
     * public void removeUpdate(DocumentEvent e) {
     * checkCodigoField(); // necessario nesse caso?
     * }
     * public void insertUpdate(DocumentEvent e) {
     * checkCodigoField();
     * }
     * public void changedUpdate(DocumentEvent e) {
     * checkCodigoField();
     * }
     * });
     * ...
     * private void checkCodigoField() {
     * String code = codigoField.getText();
     * if (code.length() >= 4) {
     * if (checkCode(code)) { // metodo para testar validade do codigo
     * quantiField.requestFocusInWindow();
     * } else {
     * Toolkit.getDefaultToolkit().beep(); // ou mensagem de erro
     * }
     * }
     * }
     */
//    /**
//     *
//     * @return AppManager _instance
//     */
//    public static JTextFieldDocumentListener getInstance()
//    {
//        return SingletonHolder._instance;
//    }
//    @SuppressWarnings ("synthetic-access")
//    private static class SingletonHolder
//    {
//        protected static final JTextFieldDocumentListener _instance = new JTextFieldDocumentListener();
//    }
}
