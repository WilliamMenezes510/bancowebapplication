/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.utils;


import br.com.conductor.app.init.Config;
import org.apache.log4j.Logger;

/**
 *
 * @author William
 */
public class ValidaCPF
{

    private static final Logger _log = Logger.getLogger(ValidaCPF.class);

    /**
     *
     * @param strCpf
     * @return
     */
    public boolean validaCpf(String strCpf)
    {
        _log.info("validandop cpf...");
        int cpf[] = new int[11], dv1 = 0, dv2 = 0;

        strCpf = strCpf.replace(".", "");
        strCpf = strCpf.replace("-", "");

        if (Config.getInstance().isDebug())
        {
            _log.info(strCpf);
        }
        if (strCpf.length() != 11)
        {
            return false;
        }

        for (int i = 0; i < 11; i++)
        {
            cpf[i] = Integer.parseInt(strCpf.substring(i, i + 1));
        }
        for (int i = 0; i < 9; i++)
        {
            dv1 += cpf[i] * (i + 1);
        }
        cpf[9] = dv1 %= 11;
        for (int i = 0; i < 10; i++)
        {
            dv2 += cpf[i] * i;
        }
        cpf[10] = dv2 %= 11;
        if (dv1 > 9)
        {
            cpf[9] = 0;
        }
        if (dv2 > 9)
        {
            cpf[10] = 0;
        }

        if (Integer.parseInt(strCpf.substring(9, 10)) != cpf[9]
            || Integer.parseInt(strCpf.substring(10, 11)) != cpf[10])
        {
            if (Config.getInstance().isDebug())
            {
                _log.info("cpf invalido.");
            }
            return false;

        }
        else
        {
            if (Config.getInstance().isDebug())
            {
                _log.info("cpf ok.");
            }
            return true;
        }
    }
}
