/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.utils;

import br.com.conductor.app.standards.MaskType;

import com.mysql.cj.util.StringUtils;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import org.apache.log4j.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.text.*;

/**
 *
 * @author William
 */
public class TextFieldUtil
{

    private static final Logger _log = Logger.getLogger(TextFieldUtil.class);

    /**
     *
     * @param entrada
     * <p/>
     * @return
     */
    public String removeNaoNumericos(String entrada)
    {
        // Padrão que caracteriza caracteres numéricos
        Pattern numericos = Pattern.compile("([0-9])");

        // Colocando o texto no padrão para ver o que encaixa
        Matcher encaixe = numericos.matcher(entrada);

        // Criando um buffer de saída, que é uma solução
        // mais otimizada do que ir concatenando uma String
        StringBuilder saida = new StringBuilder();

        // A cada número encontrado
        while (encaixe.find())
        // Adiciona-se esse número ao buffer
        {
            saida.append(encaixe.group());
        }

        // Devolvendo o buffer convertido em String
        return saida.toString();
    }

    /**
     *
     * @param f
     * @param evt
     * @param quantidade
     */
    public void limitaCaracteres(JTextField f, KeyEvent evt, int quantidade)
    {
        // Limita o JTextField A xx Caracteres
        // -----------------------------------
        f = (JTextField) evt.getComponent();
        if (f.getText().length() >= quantidade)
        {
            evt.consume();
        }
    }

    /**
     *
     * @param campo
     */
    public void permitaSomenteCaracteresSelecionados(JTextField campo)
    {
        DocumentFilter filter = new DocumentFilter()
        {
            private final String proibidas = "[\\p{Lu}a-zA-Z.]";

            @Override
            public void insertString(FilterBypass fb, int offset, String text, AttributeSet attr)
                    throws BadLocationException
            {
                fb.insertString(offset, text.replaceAll(proibidas, ""), attr);
            }

            @Override
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
                    throws BadLocationException
            {
                fb.replace(offset, length, text.replaceAll(proibidas, ""), attrs);
            }
        };

        PlainDocument doc = new PlainDocument();
        doc.setDocumentFilter(filter);

        // JTextField tf = new JTextField();
        campo.setDocument(doc);

        //JOptionPane.showMessageDialog(null, c);
    }

    /**
     * Limita m JTextField a Apenas Numeros
     *
     * @param campo
     */
    public void aceiteApenasNumeros(JTextField campo)
    {
       /* if (!StringUtils.isNumeric(campo.getText()))
        {
            try
            {
                Long.parseLong(campo.getText());
            }
            catch (NumberFormatException ex)
            {

                JOptionPane.showMessageDialog(null, "nao e permitido letras neste campo", "Informação", JOptionPane.INFORMATION_MESSAGE);
                campo.grabFocus();//tira o foco do campo
                campo.setText("");//limpa o texto
            }
        }*/
    }

    /**
     * # = qualquer numero válido 
     * U = qualquer caractere, as letras minusculas são formatadas em maiuscula.
     * L = qualquer caractere, as letras maiusculas são formatadas em minusculas. 
     * A = qualquer caractere ou numero.
     *
     * 0 = telefone 1 = cep 2 = data 3 = cpf 4 = cnpj
     *
     * @param tipo
     * <p/>
     * @return
     */
    public DefaultFormatterFactory setMascara(int tipo)
    {
        MaskFormatter mask = null;
        try
        {
            switch (tipo)
            {
               
                case MaskType.TELEFONE://telefone
                {
                    mask = new MaskFormatter("(##) ####-####");
                    mask.setPlaceholderCharacter('_');

                    break;
                }
                case MaskType.CEP://cep
                {
                    mask = new MaskFormatter("#####-###");
                    mask.setPlaceholderCharacter('_');
                    break;
                }
                case MaskType.DATA://data
                {
                    mask = new MaskFormatter("##/##/####");
                    mask.setPlaceholderCharacter('_');
                    break;
                }
                case MaskType.CPF://cpf
                {
                    mask = new MaskFormatter("###.###.###-##");
                    mask.setPlaceholderCharacter('0');
                    break;
                }
                case MaskType.CNPJ://cnpj
                {
                    mask = new MaskFormatter("##.###.###/####-##");
                    mask.setPlaceholderCharacter('0');
                    break;
                }
                case MaskType.MOEDA://moeda
                {
                    mask = new MaskFormatter(",");
                    mask.setValidCharacters("0123456789,."); // adicione os caracteres validos
                    mask.setPlaceholderCharacter('_');
                    break;
                }
                  case MaskType.SEARCH://telefone
                {
                    mask = new MaskFormatter("##########");
                    mask.setPlaceholderCharacter('_');

                    break;
                }
            }

        }
        catch (ParseException ex)
        {
            _log.fatal(ex.getMessage());

          //  ExceptionManager.ThrowException("Erro Ao Setar Mascara: ", ex);
        }
        DefaultFormatterFactory factory = new DefaultFormatterFactory(mask, mask);
        return factory;

    }
}
