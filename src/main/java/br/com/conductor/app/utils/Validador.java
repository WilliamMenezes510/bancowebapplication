/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.app.utils;


import br.com.conductor.app.init.Config;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

/**
 *
 * @author William
 */
public class Validador
{

    private static final Logger _log = Logger.getLogger(Validador.class);

    /**
     *
     * @param texto
     * @param campo
     * @return
     */
    public Boolean validaTelefone(String texto, JFormattedTextField campo)
    {
        Boolean result = false;

        int tamanho = texto.length();
        if (tamanho < 10)
        {
            JOptionPane.showMessageDialog(null, "Telefone(s) Inválidos!");
            campo.setText("");
            result = false;
        }

        return result;

    }

    /**
     *
     * @param data
     * @return
     */
    public Boolean validaData(String data)
    {

        //remove a mascara
        // String  = new TextFieldUtil().removeNaoNumericos(data);
        if (Config.getInstance().isDebug())
        {
            _log.info(data);
        }
        _log.info("validando data..");

        if (data.isEmpty())
        {
            JOptionPane.showMessageDialog(null, "data vazia!");
            return false;
        }
        else
        {

            data = data.replace("_", "");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setLenient(false);

            //valida o ano que o usuario digitou no cadastro.
            String anoString = data.replace("/", "").substring(4);

            int ano = Integer.parseInt(anoString);
            if (ano < 1900)
            {
                JOptionPane.showMessageDialog(null, "O Ano Não Pode Ser Inferior a 1900!");
                return false;
            }
            try
            {
                sdf.parse(data);
                return true;
            }
            catch (ParseException ex)
            {

                _log.fatal(ex.getMessage());
             //   ExceptionManager.ThrowException("Erro ao Validar Data: ", ex);
                return false;
            }
        }
    }

    /**
     *
     *
     * @author Dalton Camargo @modificado por William
     *
     * @param email
     * @return
     */
    public Boolean validaMail(String email)
    {
        //repassa a string em caixa baixa
        String toLowerCase = email.toLowerCase();

        _log.info("validando email....");

        //Set the email pattern string
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+"); //ATENCAO: nao pode haver espacos nesta string o pattern nao aceita.!!

        //Match the given string with the pattern
        Matcher m = p.matcher(email);

        //check whether match is found
        boolean matchFound = m.matches();
        return matchFound;
    }
}
