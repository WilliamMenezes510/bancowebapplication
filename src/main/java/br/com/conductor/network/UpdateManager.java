/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.network;


import br.com.conductor.app.init.Config;
import br.com.conductor.app.managers.PropertiesManager;
import br.com.conductor.app.standards.Paths;
import br.com.conductor.app.utils.Util;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Rayan
 */
public class UpdateManager
{

    private static final Logger _log = Logger.getLogger(UpdateManager.class);
    private double lastVersion;

//    public static void downloadFiles()
//    {
//        try
//        {
//            URL link = new URL("http://rayan.googlecode.com/svn/trunk/Sat/release/Sat.jar");
//            FileUtils.copyURLToFile(link, new File("C:\\new.jar"));
//        }
//        catch (IOException ex)
//        {
//            Logger.getLogger(UpdateManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
    /**
     *
     * @return
     */
    public double getLastVersion()
    {
        return lastVersion;
    }

    /**
     *
     * @param lastVersion
     */
    public void setLastVersion(double lastVersion)
    {

        //_log.info( "Setando Versao para: {0}", lastVersion);
        this.lastVersion = lastVersion;
    }

//        URL dl = null;
//        File fl = null;
//        String x = null;
//        try
//        {
//            fl = new File(System.getProperty("user.home").replace("\\", "/") + "/Desktop/Sat.jar");
//            dl = new URL("http://rayan.googlecode.com/svn/trunk/Sat/release/Sat.jar");
//            OutputStream os = new FileOutputStream(fl);
//            InputStream is = dl.openStream();
//            CountingOutputStream count = new CountingOutputStream(os);
//            dl.openConnection().getHeaderField("Content-Length");
//            IOUtils.copy(is, os);//begin transfer
//
//            os.close();//close streams
//            is.close();//^
//        }
//        catch (Exception e)
//        {
//            System.out.println(e);
//        }
    //}
    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
        // downloadFiles();
    }

    /**
     *
     */
    public UpdateManager()
    {
    }

    private boolean downloadUpdateFile(String url2)
    {
        if (Config.getInstance().isDebug())
        {
            _log.info("Baixando Arquivo de Versao..");
        }
        /*
         *  //trying to retrieve data from the source. If there //is no
         * connection, this line will fail //Object objData =
         * urlConnect.getContent();
         *
         */
        try
        {
            URL url = new URL(url2);
            ByteArrayOutputStream os;

            try (InputStream is = url.openStream())
            {
                os = new ByteArrayOutputStream();

                byte[] buf = new byte[4096];
                int n;
                while ((n = is.read(buf)) >= 0)
                {
                    os.write(buf, 0, n);
                }
            }
            //definindo pasta e arquivo que iremos criar
            String diretorio = Paths.getUpdateDirecory();
            String nomeArquivo = Paths.getUpdateFile();

            //criando pasta
            File pasta = new File(diretorio);
            if (pasta.exists())
            {
                pasta.delete();
            }
            else
            {
                pasta.mkdir();
            }
            //cria arquivo
            File arquivo = new File(nomeArquivo);
            if (arquivo.exists())
            {
                arquivo.delete();
            }
            else
            {
                arquivo.createNewFile();
            }

            //grava o output stream no arquivo.
            FileOutputStream fos = new FileOutputStream(arquivo);
            byte[] data = os.toByteArray();
            fos.write(data);

            //fecha o canal de dados
            os.close();
            return true;

        }
        catch (MalformedURLException e)
        {
            _log.fatal(e.getMessage());
        }
        catch (IOException e)
        {
            _log.fatal(e.getMessage());
        }
        return false;

    }

    private void readRemoteVersionFile()
    {
        double remoteVersion = 0.0;

        File arquivoUpdate = new File(Paths.getUpdateFile());
        if (!arquivoUpdate.exists())
        {
            Util.showErrorMessage("O arquivo  " + arquivoUpdate.getName() + " nao existe!");
        }
        try
        {
            InputStream is = new FileInputStream(arquivoUpdate);
            PropertiesManager arquivo = new PropertiesManager();
            arquivo.load(is);

            remoteVersion = Double.parseDouble(arquivo.getProperty("newVersion", "0.0"));

        }
        catch (FileNotFoundException ex)
        {
            _log.fatal(ex.getMessage());
            java.util.logging.Logger.getLogger(UpdateManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex)
        {
            _log.fatal(ex.getMessage());
            java.util.logging.Logger.getLogger(UpdateManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        //guarda esse valor nessa variavel , apenas por uma questao de performance ,
        //nao sera preciso executar este metodo denovo quando quisermos saber esse valor.
        setLastVersion(remoteVersion);
        // return remoteVersion;
    }

    /**
     *
     * @return
     */
    public boolean updateAvaible()
    {
        if (Config.getInstance().isEnableLog())
        {
            _log.info("Verificando se Há Atualizacoes Disponíveis...");
        }
//        if (!Config.getInstance().isLoaded())
//        {
//            Config.getInstance().load();
//        }

        if (downloadUpdateFile(Config.getInstance().getUpdateUrl() + "version.properties"))
        {
            readRemoteVersionFile();
            if (Config.getInstance().isDebug())
            {
                _log.info(String.valueOf("Versao Local: " + Config.getInstance().getSystemVersion()));
                _log.info(String.valueOf("Versao Remota: " + getLastVersion()));
            }

            return Config.getInstance().getSystemVersion() < getLastVersion();

        }
        else
        {
            Util.showErrorMessage("Problemas ao Verificar Versao no Servidor, Tente Mais Tarde.");
            return false;
        }
    }
}
