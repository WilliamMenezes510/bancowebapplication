/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity.beans;

import br.com.conductor.model.controller.PessoaController;
import br.com.conductor.model.entity.Pessoa;
import br.com.conductor.model.controller.PessoaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

/**
 *
 * @author WILLIAM
 */
@SessionScoped
@ManagedBean
public class PessoaMB implements Serializable
{

    @Inject
    private PessoaFacadeLocal fachadaPessoa;
    private final PessoaController pessoaController = new PessoaController();

    public Pessoa getPessoa()
    {
        return pessoaController.getPessoa();
    }

    public void setPessoa(Pessoa pessoa)
    {
        this.pessoaController.getPessoa();
    }

    public List<Pessoa> getPessoas()
    {
        return pessoaController.getPessoas();
    }

    public void setPessoas(List<Pessoa> pessoas)
    {
        this.pessoaController.setPessoas(pessoas);
    }

    public void adcionar(String nome, String sobrenome, String email)
    {
        this.pessoaController.adcionar(nome, sobrenome, email);

    }

    public void json()
    {
        this.pessoaController.json();

    }

    public String someOutput()
    {
        return this.pessoaController.someOutput();
    }

    public void limpar()
    {
        pessoaController.limpar();
    }
}
