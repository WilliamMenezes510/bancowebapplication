/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author William
 */
public class Funcionario extends Usuario
{

    private String cargo;

    /**
     *
     * @return
     */
    public String getCargo()
    {
        return cargo;
    }

    /**
     *
     * @param cargo
     */
    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

}
