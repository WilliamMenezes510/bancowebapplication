/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author user
 */
public class Tecnico
{

    private int _id;
    private String _nome;
    private String _sobrenome;

    public Tecnico()
    {
    }

    
    
    /**
     *
     * @param id
     * @param nome
     * @param sobrenome
     */
    public Tecnico(int id, String nome, String sobrenome)
    {
        _id = id;
        _nome = nome;
        _sobrenome = sobrenome;

    }

    public String getSobrenome()
    {
        return _sobrenome;
    }

    public void setSobrenome(String _sobrenome)
    {
        this._sobrenome = _sobrenome;
    }

    /**
     *
     * @return
     */
    public int getId()
    {
        return _id;
    }

    /**
     *
     * @param _id
     */
    public void setId(int _id)
    {
        this._id = _id;
    }

    /**
     *
     * @return
     */
    public String getNome()
    {
        return _nome;
    }

    /**
     *
     * @param _nome
     */
    public void setNome(String _nome)
    {
        this._nome = _nome;
    }

}
