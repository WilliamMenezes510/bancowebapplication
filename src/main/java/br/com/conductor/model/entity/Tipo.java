/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author William
 */
public class Tipo
{

    private int _id;
    private String _descricao;

    /**
     *
     * @param id
     * @param tipo
     */
    public Tipo(int id, String tipo)
    {
        _id = id;
        _descricao = tipo;
    }

    public Tipo()
    {
    }

    /**
     *
     * @return
     */
    public int getId()
    {
        return _id;
    }

    /**
     *
     * @param _id
     */
    public void setId(int _id)
    {
        this._id = _id;
    }

    /**
     *
     * @return
     */
    public String getDescricao()
    {
        return _descricao;
    }

    /**
     *
     * @param _tipo
     */
    public void setDescricao(String _tipo)
    {
        this._descricao = _tipo;
    }
}
