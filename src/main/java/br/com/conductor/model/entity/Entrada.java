/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author Rayan
 */
public class Entrada
{

    private int id;
    private int numEnt;
    private String dataAbertura;
    private String hora;
    private String usuario;
    private String marca;
    private String equipamento;
    private String tipoDefeito;
    private String tipoEntrada;
    private String quantidade;
    private String responsavel;
    private String defeitos;
    private String perifericos;
    private String observacoes;
    private String situacao;
    private String tecnico;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getNumEnt()
    {
        return numEnt;
    }

    public void setNumEnt(int numEnt)
    {
        this.numEnt = numEnt;
    }

    public String getDataAbertura()
    {
        return dataAbertura;
    }

    public void setDataAbertura(String dataAbertura)
    {
        this.dataAbertura = dataAbertura;
    }

    public String getHora()
    {
        return hora;
    }

    public void setHora(String hora)
    {
        this.hora = hora;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public String getMarca()
    {
        return marca;
    }

    public void setMarca(String marca)
    {
        this.marca = marca;
    }

    public String getEquipamento()
    {
        return equipamento;
    }

    public void setEquipamento(String equipamento)
    {
        this.equipamento = equipamento;
    }

    public String getTipoDefeito()
    {
        return tipoDefeito;
    }

    public void setTipoDefeito(String tipoDefeito)
    {
        this.tipoDefeito = tipoDefeito;
    }

    public String getTipoEntrada()
    {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada)
    {
        this.tipoEntrada = tipoEntrada;
    }

    public String getQuantidade()
    {
        return quantidade;
    }

    public void setQuantidade(String quantidade)
    {
        this.quantidade = quantidade;
    }

    public String getResponsavel()
    {
        return responsavel;
    }

    public void setResponsavel(String responsavel)
    {
        this.responsavel = responsavel;
    }

    public String getDefeitos()
    {
        return defeitos;
    }

    public void setDefeitos(String defeitos)
    {
        this.defeitos = defeitos;
    }

    public String getPerifericos()
    {
        return perifericos;
    }

    public void setPerifericos(String perifericos)
    {
        this.perifericos = perifericos;
    }

    public String getObservacoes()
    {
        return observacoes;
    }

    public void setObservacoes(String observacoes)
    {
        this.observacoes = observacoes;
    }

    public String getSituacao()
    {
        return situacao;
    }

    public void setSituacao(String situacao)
    {
        this.situacao = situacao;
    }

    public String getTecnico()
    {
        return tecnico;
    }

    public void setTecnico(String tecnico)
    {
        this.tecnico = tecnico;
    }

}
