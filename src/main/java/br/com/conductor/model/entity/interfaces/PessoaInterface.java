/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity.interfaces;

import br.com.conductor.model.entity.Pessoa;
import java.util.List;

/**
 *
 * @author WILLIAM
 */
public interface PessoaInterface
{

    /**
     *
     * @return
     */
    Pessoa getPessoa();

    void setPessoa(Pessoa pessoa);

    List<Pessoa> getPessoas();

    void setPessoas(List<Pessoa> pessoas);

    void adcionar(String nome, String sobrenome, String email);

    void json();

    public String someOutput();
}
