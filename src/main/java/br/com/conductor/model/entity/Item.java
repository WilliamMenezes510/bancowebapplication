/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author William
 */
public class Item
{
    /**
     * As diferenças do float para o double são:
     *
     * 1. O double tem mais precisão: pode representar mais casas decimais ou
     * números inteiros maiores; 2. O double ocupa 32 bits a mais que o float (o
     * dobro do espaço); 3. Dependendo do hardware, o cálculo de um ou outro
     * será mais rápido. Placas de vídeo geralmente operam com floats, as mais
     * novas, com doubles.
     *
     * Por incrível que pareça, o último aspecto é bastante relevante.
     * Aplicações gráficas, por exemplo, são formadas por milhares de pontos, e
     * essa diferença pode representar alguns MB em memória.
     */
    private int quantidade;
    private String descrição;
    private double valorUnitario;
    private double total;

    public Item()
    {
    }

    public Item(int quantidade, String descrição, double valorUnitario, double total)
    {
        this.quantidade = quantidade;
        this.descrição = descrição;
        this.valorUnitario = valorUnitario;
        this.total = total;
    }

    public int getQuantidade()
    {
        return quantidade;
    }

    public void setQuantidade(int quantidade)
    {
        this.quantidade = quantidade;
    }

    public String getDescrição()
    {
        return descrição;
    }

    public void setDescrição(String descrição)
    {
        this.descrição = descrição;
    }

    public double getValorUnitario()
    {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario)
    {
        this.valorUnitario = valorUnitario;
    }

    public double getTotal()
    {
        return total;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

}
