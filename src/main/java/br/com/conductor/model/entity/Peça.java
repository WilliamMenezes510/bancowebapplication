/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author William
 */
public class Peça
{
    int id;
    Tipo tipo;
    Soquete soquete;
    String modelo;
    String fabricante;
    float precoCusto;
    float precoVenda;
    int margemLucro;
    
    public Peça(int id, Tipo tipo,Soquete soquete,String modelo, String fabricante, float precoCusto, float precoVenda, int margemLucro)
    {
        this.id = id;
        this.tipo = tipo;
        this.soquete=soquete;
        this.modelo=modelo;
        this.fabricante = fabricante;
        this.precoCusto = precoCusto;
        this.precoVenda = precoVenda;
        this.margemLucro = margemLucro;
    }
    
    public Peça()
    {
     
    }

    public Soquete getSoquete()
    {
        return soquete;
    }

    public void setSoquete(Soquete soquete)
    {
        this.soquete = soquete;
    }

    public String getModelo()
    {
        return modelo;
    }

    public void setModelo(String modelo)
    {
        this.modelo = modelo;
    }
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public Tipo getTipo()
    {
        return tipo;
    }
    
    public void setTipo(Tipo tipo)
    {
       
        this.tipo = tipo;
    }
    
    public String getFabricante()
    {
        return fabricante;
    }
    
    public void setFabricante(String Fabricante)
    {
        this.fabricante = Fabricante;
    }
    
    public float getPrecoCusto()
    {
        return precoCusto;
    }
    
    public void setPrecoCusto(float precoCusto)
    {
        this.precoCusto = precoCusto;
    }
    
    public float getPrecoVenda()
    {
        return precoVenda;
    }
    
    public void setPrecoVenda(float precoVenda)
    {
        this.precoVenda = precoVenda;
    }
    
    public int getMargemLucro()
    {
        return margemLucro;
    }
    
    public void setMargemLucro(int margemLucro)
    {
        this.margemLucro = margemLucro;
    }
    
}
