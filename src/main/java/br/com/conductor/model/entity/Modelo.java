/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author William
 */
public class Modelo
{
    int id;
    String descricao;
    Fabricante fabricante;

    public Modelo()
    {
    }

    public Modelo(int id, String descricao, Fabricante fabricante)
    {
        this.id = id;
        this.descricao = descricao;
        this.fabricante = fabricante;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public Fabricante getFabricante()
    {
        return fabricante;
    }

    public void setFabricante(Fabricante fabricante)
    {
        this.fabricante = fabricante;
    }
    
    
}
