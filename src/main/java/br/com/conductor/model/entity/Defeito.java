/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author William
 */
public class Defeito
{

    int id;
    String tipo;
    String descricao;

    public Defeito()
    {

    }

    public Defeito(int id, String tipo, String descricao)
    {
        this.id = id;
        this.tipo = tipo;
        this.descricao = descricao;
    }

    public String getTipo()
    {
        return tipo;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

}
