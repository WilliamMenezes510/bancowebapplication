/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.entity;

/**
 *
 * @author William
 */
public class Saida
{

    int id;
    String numEntrada;
    String cliente;
    String tecnico;
    String equipamento;
    String situacao;
    String dataConclusao;
    String defeitos;
    String problemas;
    String pecas;
    String servicos;

    public Saida()
    {
    }

    public Saida(int id, String numEntrada, String cliente, String tecnico, String equipamento, String situacao, String dataConclusao, String defeitos, String problemas, String pecas, String servicos)
    {
        this.id = id;
        this.numEntrada = numEntrada;
        this.cliente = cliente;
        this.tecnico = tecnico;
        this.equipamento = equipamento;
        this.situacao = situacao;
        this.dataConclusao = dataConclusao;
        this.defeitos = defeitos;
        this.problemas = problemas;
        this.pecas = pecas;
        this.servicos = servicos;
    }


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNumEntrada()
    {
        return numEntrada;
    }

    public void setNumEntrada(String numEntrada)
    {
        this.numEntrada = numEntrada;
    }

    public String getCliente()
    {
        return cliente;
    }

    public void setCliente(String cliente)
    {
        this.cliente = cliente;
    }

    public String getTecnico()
    {
        return tecnico;
    }

    public void setTecnico(String tecnico)
    {
        this.tecnico = tecnico;
    }

    public String getEquipamento()
    {
        return equipamento;
    }

    public void setEquipamento(String equipamento)
    {
        this.equipamento = equipamento;
    }

    public String getSituacao()
    {
        return situacao;
    }

    public void setSituacao(String situacao)
    {
        this.situacao = situacao;
    }

    public String getDataConclusao()
    {
        return dataConclusao;
    }

    public void setDataConclusao(String dataConclusao)
    {
        this.dataConclusao = dataConclusao;
    }

    public String getDefeitos()
    {
        return defeitos;
    }

    public void setDefeitos(String defeitos)
    {
        this.defeitos = defeitos;
    }

    public String getProblemas()
    {
        return problemas;
    }

    public void setProblemas(String problemas)
    {
        this.problemas = problemas;
    }

    public String getPeca()
    {
        return pecas;
    }

    public void setPeca(String peca)
    {
        this.pecas = peca;
    }

    public String getServico()
    {
        return servicos;
    }

    public void setServico(String servicos)
    {
        this.servicos = servicos;
    }

}
