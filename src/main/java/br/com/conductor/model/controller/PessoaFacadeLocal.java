/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.controller;

import br.com.conductor.model.entity.Pessoa;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author WILLIAM
 */
@Local
public interface PessoaFacadeLocal
{
    void edit(Pessoa pessoa);

    void remove(Pessoa pessoa);

    Pessoa find(Object id);

    List<Pessoa> findAll();

    List<Pessoa> findRange(int[] range);

    int count();
}
