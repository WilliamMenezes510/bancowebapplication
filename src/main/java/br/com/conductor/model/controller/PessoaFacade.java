/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.controller;

import br.com.conductor.model.entity.Pessoa;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author WILLIAM
 */
@Stateless
public class PessoaFacade extends AbstractFacade<Pessoa> implements PessoaFacadeLocal
{

    @PersistenceContext(unitName = "br.com.conductor_BancoConductor_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    public PessoaFacade()
    {
        super(Pessoa.class);
    }
  
}
