/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.controller;

import br.com.conductor.model.entity.Pessoa;
import br.com.conductor.model.entity.beans.PessoaMB;
import br.com.conductor.network.WebServiceClient;
import br.com.conductor.app.standards.Standards;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WILLIAM
 */
public class PessoaController
{
 Pessoa pessoa;
    
  public PessoaController()
    {
        pessoa = new Pessoa();
    }

    public Pessoa getPessoa()
    {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa)
    {
        this.pessoa = pessoa;
    }

    public List<Pessoa> getPessoas()
    {
        WebServiceClient http = null;
        String json = null;
        List<Pessoa> listaPessoas = new ArrayList();
        try
        {
            RESOURCE = "Pessoa/List";
            http = new WebServiceClient();
            json = http.sendGet(Standards.WEBSERVICE + RESOURCE);
        }
        catch (Exception ex)
        {
            Logger.getLogger(PessoaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        Gson g = new Gson();

        Type listType = new TypeToken<List<Pessoa>>()
        {
        }.getType();
        //JSONArray ja = new JSONArray();
        listaPessoas.addAll(g.fromJson(json, listType));

//        for (int i = 0; i < ja.length(); i++)
//        {
//            JSONObject jsObj = ja.getJSONObject(i);
//            String id = jsObj.getString("id");
//            String nome = jsObj.getString("nome");
//            String snome = jsObj.getString("sobrenome");
//            String mail = jsObj.getString("email");
//            Pessoa p = new Pessoa();
//            p.setNome(nome);
//            p.setId(Long.valueOf(id));
//            p.setSobrenome(snome);
//            p.setEmail(mail);
//            listaPessoas.add(p);
//        }
        // p = g.fromJson(json, usuarioType);
//       p.setNome("45");
//       p.setEmail("45");
//       p.setId(Long.valueOf("114"));
//       p.setSobrenome("45");
//        p2.setNome("45");
//       p2.setEmail("45");
//       p2.setId(Long.valueOf("114"));
//       p2.setSobrenome("45");
//       listaPessoas.add(p);
//       listaPessoas.add(p2);
        return listaPessoas;
    }

    public void setPessoas(List<Pessoa> pessoas)
    {
        this.pessoas = pessoas;
    }

    private List<Pessoa> pessoas = new ArrayList();
    private String RESOURCE;

    public void adcionar(String nome, String sobrenome, String email)
    {
        try
        {
            RESOURCE = "Pessoa/inserir/" + nome + "/" + sobrenome + "/" + email;
            WebServiceClient http = new WebServiceClient();
            http.sendGet(Standards.WEBSERVICE + RESOURCE);
        }
        catch (Exception ex)
        {
            Logger.getLogger(PessoaMB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void json()
    {
        try
        {
            RESOURCE = "Pessoa/";
            WebServiceClient http = new WebServiceClient();
            http.sendGet(Standards.WEBSERVICE + RESOURCE);
        }
        catch (Exception ex)
        {
            Logger.getLogger(PessoaMB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String someOutput()
    {
        return "" + System.currentTimeMillis();
    }

    public void limpar()
    {
        pessoa = new Pessoa();
    }

}
