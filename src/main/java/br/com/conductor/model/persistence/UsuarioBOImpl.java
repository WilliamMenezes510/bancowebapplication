/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.model.persistence;

import br.com.conductor.model.entity.UsuarioJPAOLD;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 *
 * @author WILLIAM
 */
public class UsuarioBOImpl
{
//    // Verifica se usuário existe ou se pode logar
//       public Usuario isUsuarioReadyToLogin(String email, String senha) {
//             try {
//                    email = email.toLowerCase().trim();
//                //    logger.info("Verificando login do usuário " + email);
//                    List retorno = dao.findByNamedQuery(
//                                  Usuario.FIND_BY_EMAIL_SENHA,
//                                  new NamedParams("email", email
//                                               .trim(), "senha", convertStringToMd5(senha)));
//  
//                    if (retorno.size() == 1) {
//                           Usuario userFound = (Usuario) retorno.get(0);
//                           return userFound;
//                    }
//  
//                    return null;
//             } catch (DAOException e) {
//                    e.printStackTrace();
//                    throw new BOException(e.getMessage());
//             }
//       }
       
       private String convertStringToMd5(String valor) {
             MessageDigest mDigest;
             try { 
                    //Instanciamos o nosso HASH MD5, poderíamos usar outro como
                    //SHA, por exemplo, mas optamos por MD5.
                    mDigest = MessageDigest.getInstance("MD5");
                     
                    //Convert a String valor para um array de bytes em MD5
                    byte[] valorMD5 = mDigest.digest(valor.getBytes("UTF-8"));
                     
                    //Convertemos os bytes para hexadecimal, assim podemos salvar
                    //no banco para posterior comparação se senhas
                    StringBuffer sb = new StringBuffer();
                    for (byte b : valorMD5){
                           sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1,3));
                    }
  
                    return sb.toString();
                     
             } catch (NoSuchAlgorithmException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return null;
             } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return null;
             }
       }
}
